from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_populate, diplomacy_eval, diplomacy_solve

class TestDiplomacy(TestCase):

  # test read
  def test_read_1(self):
    r = StringIO('A Madrid Hold\n')
    self.assertEqual(diplomacy_read(r), [['A', 'Madrid', 'Hold']])

  def test_read_2(self):
    r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
    self.assertEqual(diplomacy_read(r), [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']])

  def test_read_3(self):
    r = StringIO('C London Support B\nB Barcelona Move Madrid\nA Madrid Hold\n')
    self.assertEqual(diplomacy_read(r), [['C', 'London', 'Support', 'B'], ['B', 'Barcelona', 'Move', 'Madrid'], ['A', 'Madrid', 'Hold']])


  # test eval
  def test_eval_1(self):
    r = StringIO('A Madrid Hold\n')
    moves = diplomacy_read(r)
    pop = diplomacy_populate(moves)       # helper function
    self.assertEqual(diplomacy_eval(pop), [['A', 'Madrid']])

  def test_eval_2(self):
    r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
    moves = diplomacy_read(r)
    pop = diplomacy_populate(moves)       # helper function
    self.assertEqual(diplomacy_eval(pop), [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']])

  def test_eval_3(self):
    r = StringIO('C London Support B\nB Barcelona Move Madrid\nA Madrid Hold\n')
    moves = diplomacy_read(r)
    pop = diplomacy_populate(moves)       # helper function
    self.assertEqual(diplomacy_eval(pop), [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']])


  # test solve
  def test_solve_1(self):
    r = StringIO('A Madrid Hold\n')
    w = StringIO()
    diplomacy_solve(r, w)
    self.assertEqual(w.getvalue(), 'A Madrid\n')

  def test_solve_2(self):
    r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
    w = StringIO()
    diplomacy_solve(r, w)
    self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

  def test_solve_3(self):
    r = StringIO('C London Support B\nB Barcelona Move Madrid\nA Madrid Hold\n')
    w = StringIO()
    diplomacy_solve(r, w)
    self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

  def test_solve_4(self):
    r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Dallas Move London\nF Houston Support E\n')
    w = StringIO()
    diplomacy_solve(r, w)
    self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\nE London\nF Houston\n')

if __name__ == '__main__': # pragma: no cover
  main()
